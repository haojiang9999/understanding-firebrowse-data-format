import pandas as pd

# `confirm-preprocess-origin.py` confirms that the `raw_counts` and
# `scaled_estimate` files in `Preprocess` dir are extracted from `RSEM_genes`
# file.

f1 = './data/gdac.broadinstitute.org_KICH.Merge_rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes__data.Level_3.2016012800.0.0/KICH.rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes__data.data.txt'
f2 = './data/gdac.broadinstitute.org_KICH.mRNAseq_Preprocess.Level_3.2016012800.0.0/KICH.uncv2.mRNAseq_raw_counts.txt'
f3 = './data/gdac.broadinstitute.org_KICH.mRNAseq_Preprocess.Level_3.2016012800.0.0/KICH.uncv2.mRNAseq_scaled_estimate.txt'

df1 = pd.read_csv(f1, sep='\t', low_memory=False)
df2 = pd.read_csv(f2, sep='\t', low_memory=False)
df3 = pd.read_csv(f3, sep='\t', low_memory=False)

raw_count1 = df1['TCGA-KL-8323-01A-21R-2315-07'].drop(0).apply(float).astype(int).values
raw_count2 = df2['TCGA-KL-8323-01'].astype(int).values

assert (raw_count1 == raw_count2).all()

scaled_est1 = df1['TCGA-KL-8323-01A-21R-2315-07.1'].drop(0).apply(float).values
scaled_est3 = df3['TCGA-KL-8323-01'].astype(float).values

assert ((scaled_est1 - scaled_est3) < 1e-6).all()
